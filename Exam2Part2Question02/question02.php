<?php
session_start();

$nameErr='';


if($_SERVER["REQUEST_METHOD"]=="POST"){

  if(empty($_POST["user"])){
    $nameErr = "Required field";
  } else {

    $_SESSION["user"] = $_POST["user"];
    header('location: process.php');
  }

}
?>
<html>
  <head>
    <title> Question 2 - question02.php</title>
    <link href="css/styles.css" rel="stylesheet" />

  </head>
  <body>
    <main id ="main">
<h1>Best Pics </h1>
<form action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method = "post">
  Name: <input type = "text" name = "user"> <br/>
  <span class="error">* <?php echo $nameErr;?></span>

  <h2>Check your favourite Pics</h2>
  <?php include "functions.inc.php"?>

  <button name = "submit" class = "button" type = "submit">Click here to Submit </button>

</form>
<footer>
  <p>Michael Ofei, Copyright &copy; 2020 - IT353 - Web Development Technologies </p>

    </footer>
    </main>
  </body>
</html>
