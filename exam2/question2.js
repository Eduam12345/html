var client= [{firstName:"Alice", lastName:"Carr", yearOfBirth:1985,city:"Normal"},{firstName:"Bob", lastName:"Doe", yearOfBirth:1991,city:"Bloomington"},{firstName:"Danny", lastName:"Press", yearOfBirth:2001,city:"Chicago"}];

function computeAge(c){
    return 2020-c.yearOfBirth;
}
function displayClients(){
    var i;
    for(i=0;i<client.length;i++){

        document.writeln((i+1)+". Last Name: "+client[i].lastName+", First Name: ",client[i].firstName+", Age: "+computeAge(client[i]));
        document.writeln("<br><br>");
    }
}
