
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Activity 11 - Database</title>
</head>
<body>
  <header>
    <h1>Activity 11 - Database</h1>
  </header>
  <?php
  try{

    // Place your code to connect to the database here
    $connString="mysql:host=localhost;dbname=testDB";
    $user="vmuser";
    $pass="1234";
    $pdo = new PDO($connString,$user,$pass);
 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 $sql = "SELECT * FROM costumers ORDER BY last_name";
 $result = $pdo->query($sql);
 while ($row = $result->fetch()) {
 echo $row['costID'] . " - " . $row['last_name'] . "<br/>";
 }
$pdo = null;

  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }
  ?>

  <footer>
    <p>Michael Ofei, Copyright &copy; 2020 - IT353 - Web Development Technologies </p>
      <script>
          document.write("Last Modified: "+ document.lastModified);
       </script>
  </footer>
</body>
</html>
