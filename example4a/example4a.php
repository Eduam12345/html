<?php
session_start();
$nameErr='';
if($_SERVER["REQUEST_METHOD"]=="POST"){
  if(empty($_POST["user"])||empty($_POST["pass"])){
   $nameErr="Required field";
  }else{
//  echo "redirect";
$_SESSION["user"]=$_POST["user"];
header('location:process4a.php');
  }
}
 ?>
<html>
<head>
  <title> Example 4a - PHP </title>
</head>
<body>

  <main id ="main">
    <h1> Example 4a - PHP </h1>

    <form action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method = "post">
      Name: <input type = "text" name = "user"> <br/>
      <span class="error">* <?php echo $nameErr;?></span>
      <br>
      Password: <input type = "password" name = "pass"> <br/>
      <span class="error">* <?php echo $nameErr;?></span>
      <br>


      <button name = "submit" type = "submit"> Login </button>

    </form>
    <footer>
      <p>Michael Ofei, Copyright &copy; 2020 - IT353 - Web Development Technologies </p>
        <script>
            document.write("Last Modified: "+ document.lastModified);
         </script>
    </footer>

  </main>
</body>
</html>
