

<?php
session_start();
 ?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Cryptic Critics</title>
  <link rel = "stylesheet" href= "project.css" />
</head>
<body>
<header>
  <img src = "logo.jpg" id = "logo" alt="logo">
</header>
<div id = "top">
  <nav>
    <ul>
      <li><a href="ratings.html">Reviews</a></li>
      <li><a href="Signup.html">Sign Up</a></li>
      <li><a href="Project1.php">Login</a></li>
      <p> Logged in as <?php echo $_SESSION["user"]?></p>
    </ul>
  </nav>
  <p id = "search">
    <input type ="text" id ="find" name="search" placeholder="FIND MOVIE TO REVIEW" >
    <input type="button" id ="button" value="Search">
  </p>
</div>
<div>
  <h2>Popular Movies</h2>
  <img src = "order.jpg" class="popular" alt ="Harry Potter">
  <img src = "avengers.jpg" class="popular" alt="Avengers">
  <img src = "hunger.jpg" class="popular" alt="Hunger Games">
  <img src = "dark.jpg" class="popular" alt="Dark Knight">
</div>

<footer>
  <p>Alexis Smith, Michael Ofei, Crystal Lamas Copyright &copy; 2020 - IT353 - Web Development Technologies </p>
  <script>
    document.write("Last Modified: "+ document.lastModified);
    </script>
</footer>
</body>
</html>
