<?php
session_start();
$nameErr='';
if($_SERVER["REQUEST_METHOD"]=="POST"){
  if (empty($_POST["user"]) || empty($_POST["pass"])){
    $nameErr="Required field";
  }else{
//  echo "redirect";
$_SESSION["user"]=$_POST["user"];
header('location: out.php');
  }
}
 ?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
  <title>Login</title>
  <link rel="stylesheet" href="homStyle.css"/>
  <script type="text/javascript" src="home.js"></script>
</head>
<body>
  <main id ="main">
  <header>
    <h2> Welcome to Cryptic Critic</h2>
  </header>
    <p>Please Log in to Search movie data base</p>
    <fieldset>
      <legend>Log in </legend>

      <form action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method = "post">
        Name: <input type = "text" name = "user" required> <br/>
        <span class="error">* <?php echo $nameErr;?></span>
        <br>
        Password: <input type = "password" name = "pass" required> <br/>
        <span class="error">* <?php echo $nameErr;?></span>
        <br>
          <a href="#">Forgot your password?</a>
          </br>
          </br>
          <button type ="submit">Log in </button>
<span class="psw"><h5>NEW TO CRYPTIC CRITIC? <a href="Signup.html">SIGN UP</a></span></h5>
 </div>
      </form>
    </fieldset>
  </main>
</body>

</html>
