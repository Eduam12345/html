var cities = [
  { city: "Bloomington, IL",
    latitude: "40.4842° N, 88.9937° W",
    population: "77,962",
    elevation: "797 feet (243 m) above sea level",
  },
   { city: "Normal, IL",
     latitude: "40.5142° N, 88.9906° W",
     population: "54,742",
     elevation: "869 feet (265 m) above sea level",
   },
   { city: "Boulder, CO",
     latitude: "40.0150° N, 105.2705° W",
     population: "107,353",
     elevation: "5,430 feet (1,655 m) above sea level",
   },
   { city: "New Orleans, LA",
     latitude: "29.9511° N, 90.0715° W",
     population: "391,006",
     elevation: "	−6.5 to 20 ft (−2 to 6 m) above sea level",
   }
];

