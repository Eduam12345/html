
/*
Name:
Course:
Homework 4

Write your code here
*/
/*function setBackground(e) {
if (e.type == "focus") {

}
else if (e.type == "blur") {

}
}
*/
function setBackground(e)
{
  if(e.type == "focus")
  {
    e.target.style.backgroundColor = "#FFE393";
  }

  else if(e.type == "blur")
  {
    e.target.style.backgroundColor = "white";
  }
}

window.addEventListener("load", function()
{
  // Highlight boxes when they are selected
  var hilightableSelector = document.getElementsByClassName("hilightable");

  for(i = 0; i < hilightableSelector.length; i++)
  {
    hilightableSelector[i].addEventListener("focus", setBackground);
    hilightableSelector[i].addEventListener("blur", setBackground);
  }

  // Highlight required fields on submit
  document.getElementById('mainForm').addEventListener("submit", function(e)
  {
    var hilightableRequired = document.getElementById('mainForm').getElementsByClassName('hilightable required');

    for(i = 0; i < hilightableRequired.length; i++)
    {

      if(hilightableRequired[i].value == "" || hilightableRequired[i].value == null)
      {

        // Cancel submission of the form
        e.preventDefault();

        hilightableRequired[i].style.backgroundColor = "red";
        hilightableRequired[i].classList.add("error");
        hilightableRequired[i].focus();
      }
else{
  hilightableRequired[i].style.backgroundColor = "white";
  hilightableRequired[i].classList.remove("error");
  hilightableRequired[i].focus();
}
    }
  })

  // Clear errors when clear button is pressed
  for(i = 0; i < hilightableRequired.length; i++)
  {
    document.getElementById('mainForm').addEventListener("reset", function(e)
    {
      hilightableRequired[i].classList.remove("error");
      hilightableRequired[i].focus();
    })
  }



});
